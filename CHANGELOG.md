# Changelog

All notable changes to this project will be documented in this file.

## [1.2.15] - 2025-03-13

### Deps

- Bump transitive dependencies
- Bump direct dependencies

## [1.2.14] - 2025-02-05

### 🐛 Bug Fixes

- *(README)* Add AFRL SPR case number
- *(license)* MIT License

### Deps

- Bump go to v1.23.5
- Dump direct dependencies

