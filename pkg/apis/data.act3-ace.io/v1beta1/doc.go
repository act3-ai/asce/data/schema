// +k8s:conversion-gen=gitlab.com/act3-ai/asce/data/schema/pkg/apis/bottle.data.act3-ace.io

// +k8s:defaulter-gen=TypeMeta
// +k8s:defaulter-gen-input=gitlab.com/act3-ai/asce/data/schema/pkg/apis/data.act3-ace.io/v1beta1

// +kubebuilder:object:generate=true
// +groupName=bottle.data.act3-ace.io

// Package v1beta1 provides the Bottle types used ACE Data Bottles
package v1beta1
