// +k8s:conversion-gen=gitlab.com/act3-ai/asce/data/schema/pkg/apis/bottle.data.act3-ace.io
// +kubebuilder:object:generate=true
// +groupName=bottle.data.act3-ace.io

// Package v1alpha4 provides the Bottle types used ACE Data Bottles
package v1alpha4
